hhs.casestudy.AboutImageActivity -> hhs.casestudy.AboutImageActivity:
    android.widget.TextView txtDimensions -> a
    android.widget.TextView txtLastModified -> b
    android.widget.TextView txtSize -> c
    android.widget.TextView txtImageType -> d
    android.widget.Button btnClose -> e
    void onCreate(android.os.Bundle) -> onCreate
    void onClick(android.view.View) -> onClick
hhs.casestudy.AboutTctActivity -> hhs.casestudy.AboutTctActivity:
    android.widget.TextView txtTitle -> a
    android.widget.TextView txtContent -> b
    android.widget.Button btnClose -> c
    void onCreate(android.os.Bundle) -> onCreate
    void onClick(android.view.View) -> onClick
hhs.casestudy.BitmapWorkerTask -> hhs.casestudy.a:
    java.lang.Object doInBackground(java.lang.Object[]) -> doInBackground
hhs.casestudy.EditImageActivity -> hhs.casestudy.EditImageActivity:
    android.view.Menu mainMenu -> a
    android.widget.ImageView mainImageView -> b
    android.widget.LinearLayout linearLayoutToolsSet -> c
    android.widget.TextView txtToolsSetName -> d
    android.widget.Button btnOk -> e
    android.widget.Button btnCancel -> f
    android.widget.LinearLayout linearLayoutTransformTools -> g
    android.widget.ImageButton ibtnTransform -> h
    android.widget.ImageButton ibtnRotateLeft -> i
    android.widget.ImageButton ibtnRotateRight -> j
    android.widget.ImageButton ibtnFlipHozirontal -> k
    android.widget.ImageButton ibtnFlipVertical -> l
    android.widget.TextView txtTranformDegrees -> m
    android.widget.SeekBar barTransformDegrees -> n
    android.widget.LinearLayout linearLayoutBrightnesTools -> o
    android.widget.ImageButton ibtnBrightnessContrast -> p
    android.widget.TextView txtBightness -> q
    android.widget.SeekBar barBrightness -> r
    android.widget.TextView txtContrast -> s
    android.widget.SeekBar barContrast -> t
    android.widget.LinearLayout linearLayoutHueSaturation -> u
    android.widget.ImageButton ibtnHueSaturation -> v
    android.widget.TextView txtHue -> w
    android.widget.SeekBar barHue -> x
    android.widget.TextView txtHueLightness -> y
    android.widget.SeekBar barHueLightness -> z
    android.widget.TextView txtSaturation -> A
    android.widget.SeekBar barSaturation -> B
    android.widget.LinearLayout currentLayout -> C
    int modificationCount -> D
    android.graphics.Bitmap currentBitmap -> E
    android.graphics.Bitmap tempBitmap -> F
    java.util.Queue historyQueue -> G
    int currentHistory -> H
    java.lang.Boolean isCloseCommand -> I
    android.content.Context mContext -> J
    void onCreate(android.os.Bundle) -> onCreate
    void onClick(android.view.View) -> onClick
    void previewEffect() -> a
    boolean onOptionsItemSelected(android.view.MenuItem) -> onOptionsItemSelected
    void saveAs() -> b
    void undo() -> c
    void redo() -> d
    void saveBitmapToHistoryQueue() -> e
    void getAndshowSelectedImage() -> f
    boolean onCreateOptionsMenu(android.view.Menu) -> onCreateOptionsMenu
    void onProgressChanged(android.widget.SeekBar,int,boolean) -> onProgressChanged
    void onStartTrackingTouch(android.widget.SeekBar) -> onStartTrackingTouch
    void onStopTrackingTouch(android.widget.SeekBar) -> onStopTrackingTouch
    void access$0(hhs.casestudy.EditImageActivity,java.lang.Boolean) -> a
    void access$1(hhs.casestudy.EditImageActivity) -> a
    android.content.Context access$2(hhs.casestudy.EditImageActivity) -> b
    android.graphics.Bitmap access$3(hhs.casestudy.EditImageActivity) -> c
    void access$4(hhs.casestudy.EditImageActivity,int) -> a
    java.lang.Boolean access$5(hhs.casestudy.EditImageActivity) -> d
hhs.casestudy.EditImageActivity$1 -> hhs.casestudy.b:
    hhs.casestudy.EditImageActivity this$0 -> a
    void onClick(android.content.DialogInterface,int) -> onClick
hhs.casestudy.EditImageActivity$2 -> hhs.casestudy.c:
    hhs.casestudy.EditImageActivity this$0 -> a
    void onClick(android.content.DialogInterface,int) -> onClick
hhs.casestudy.EditImageActivity$3 -> hhs.casestudy.d:
    void onClick(android.content.DialogInterface,int) -> onClick
hhs.casestudy.EditImageActivity$4 -> hhs.casestudy.e:
    android.widget.EditText val$etxtCompressQuality -> a
    void onProgressChanged(android.widget.SeekBar,int,boolean) -> onProgressChanged
    void onStartTrackingTouch(android.widget.SeekBar) -> onStartTrackingTouch
    void onStopTrackingTouch(android.widget.SeekBar) -> onStopTrackingTouch
hhs.casestudy.EditImageActivity$5 -> hhs.casestudy.f:
    hhs.casestudy.EditImageActivity this$0 -> a
    android.widget.EditText val$etxtFileName -> b
    android.widget.Spinner val$spinnerImgTypes -> c
    android.widget.SeekBar val$seekBarCompressQuality -> d
    android.app.Dialog val$saveAsDialog -> e
    void onClick(android.view.View) -> onClick
hhs.casestudy.EditImageActivity$6 -> hhs.casestudy.g:
    android.app.Dialog val$saveAsDialog -> a
    void onClick(android.view.View) -> onClick
hhs.casestudy.GridAdapter -> hhs.casestudy.h:
    int getCount() -> getCount
    java.lang.Object getItem(int) -> getItem
    long getItemId(int) -> getItemId
    android.view.View getView(int,android.view.View,android.view.ViewGroup) -> getView
hhs.casestudy.ImageInfo -> hhs.casestudy.i:
    java.lang.String name -> a
    java.lang.String lastModified -> b
    int width -> c
    int height -> d
    java.lang.String type -> e
    double size -> f
hhs.casestudy.ImagePathInfo -> hhs.casestudy.j:
    java.lang.String path -> a
    java.lang.String fileName -> b
hhs.casestudy.ImagesManager -> hhs.casestudy.k:
    hhs.casestudy.ImagesManager mInstance -> a
    java.util.Vector mImagesPathInfo -> b
    java.lang.Boolean[] mVisibleImages -> c
    int mInvisibleImageCount -> d
    java.util.Vector mBitmaps -> e
    int mSelectedImageIndex -> f
    android.graphics.Bitmap mSelectedBitmap -> g
    hhs.casestudy.ImagesManager getInstance() -> a
    int getVisibleImagesCount() -> b
    int getSize() -> c
    java.lang.Boolean[] getVisibleImagesArray() -> d
    java.util.Vector getImagesPathVector() -> e
    int getImagesPathFromSDCard() -> f
    int getActualPosition(int) -> e
    void hideImage(int) -> a
    void unhideImage(int) -> b
    hhs.casestudy.ImagePathInfo getImagePathInfo(int) -> c
    android.graphics.Bitmap getBitmapFromFile(int,int,int) -> a
    void selectImage(int) -> d
    void selectNextImage() -> g
    void selectPreviousImage() -> h
    hhs.casestudy.ImagePathInfo getSelectedImagePathInfo() -> i
    android.graphics.Bitmap getSelectedBitmap(int,int,java.lang.Boolean) -> a
    java.lang.Boolean deleteSelectedImage() -> j
    void RecycleSelectedImage() -> k
hhs.casestudy.ListAdapter -> hhs.casestudy.l:
    int getCount() -> getCount
    java.lang.Object getItem(int) -> getItem
    long getItemId(int) -> getItemId
    android.view.View getView(int,android.view.View,android.view.ViewGroup) -> getView
hhs.casestudy.MainActivity -> hhs.casestudy.MainActivity:
    android.widget.ListView mListView -> b
    hhs.casestudy.ListAdapter mListAdapter -> c
    android.widget.GridView mGridView -> d
    hhs.casestudy.GridAdapter mGridAdapter -> e
    hhs.casestudy.ImagesManager imagesManager -> a
    android.content.Context mContext -> f
    void onCreate(android.os.Bundle) -> onCreate
    boolean onCreateOptionsMenu(android.view.Menu) -> onCreateOptionsMenu
    void onResume() -> onResume
    boolean onOptionsItemSelected(android.view.MenuItem) -> onOptionsItemSelected
    void onClick(android.view.View) -> onClick
    void onItemClick(android.widget.AdapterView,android.view.View,int,long) -> onItemClick
    void onPause() -> onPause
    void onDestroy() -> onDestroy
    hhs.casestudy.ListAdapter access$0(hhs.casestudy.MainActivity) -> a
    hhs.casestudy.GridAdapter access$1(hhs.casestudy.MainActivity) -> b
hhs.casestudy.MainActivity$1 -> hhs.casestudy.m:
    hhs.casestudy.MainActivity this$0 -> a
    android.widget.SearchView val$searchView -> b
    boolean onQueryTextSubmit(java.lang.String) -> onQueryTextSubmit
    boolean onQueryTextChange(java.lang.String) -> onQueryTextChange
hhs.casestudy.SlideShowActivity -> hhs.casestudy.SlideShowActivity:
    android.widget.ImageView imageView -> b
    android.widget.ImageButton btnNextImage -> c
    android.widget.ImageButton btnPreviousImage -> d
    android.widget.ImageButton btnPlayPause -> e
    android.graphics.Bitmap bitmap -> f
    hhs.casestudy.ImagePathInfo imagePathInfo -> g
    byte slideShowState -> h
    int mSlideShowTime -> i
    android.content.Context mContext -> j
    android.view.Menu mainMenu -> k
    android.os.Handler mHander -> a
    java.lang.Thread mThread -> l
    java.lang.Runnable mRunable -> m
    void onCreate(android.os.Bundle) -> onCreate
    void onClick(android.view.View) -> onClick
    boolean onOptionsItemSelected(android.view.MenuItem) -> onOptionsItemSelected
    void getSelectedBitmap() -> a
    boolean onCreateOptionsMenu(android.view.Menu) -> onCreateOptionsMenu
    byte access$0(hhs.casestudy.SlideShowActivity) -> a
    int access$1(hhs.casestudy.SlideShowActivity) -> b
    android.content.Context access$2(hhs.casestudy.SlideShowActivity) -> c
hhs.casestudy.SlideShowActivity$1 -> hhs.casestudy.n:
    hhs.casestudy.SlideShowActivity this$0 -> a
    void handleMessage(android.os.Message) -> handleMessage
hhs.casestudy.SlideShowActivity$2 -> hhs.casestudy.o:
    hhs.casestudy.SlideShowActivity this$0 -> a
    void run() -> run
hhs.casestudy.SlideShowActivity$3 -> hhs.casestudy.p:
    hhs.casestudy.SlideShowActivity this$0 -> a
    java.lang.String val$imageName -> b
    void onClick(android.content.DialogInterface,int) -> onClick
hhs.casestudy.SlideShowActivity$4 -> hhs.casestudy.q:
    void onClick(android.content.DialogInterface,int) -> onClick
hhs.casestudy.TctFile -> hhs.casestudy.r:
    java.lang.String[] strImageTypes -> a
    void getAllImagesPathFromSDCard(java.util.Vector) -> a
    void getAllImagesPathFromFile(java.util.Vector,java.io.File) -> a
    java.lang.String getDirectory(java.lang.String) -> a
    java.lang.Boolean delete(java.lang.String) -> b
hhs.casestudy.TctImage -> hhs.casestudy.s:
    hhs.casestudy.ImageInfo getImageInfo(java.lang.String) -> a
    android.graphics.Bitmap decodeSampledBitmap(java.lang.String,int,int) -> a
    android.graphics.Bitmap rotateBitmap(android.graphics.Bitmap,int) -> a
    android.graphics.Bitmap flipBitmap(android.graphics.Bitmap,int,int) -> a
    android.graphics.Bitmap Brighten(android.graphics.Bitmap,int) -> b
    android.graphics.Bitmap setSaturation$16db4d5b(android.graphics.Bitmap,float,float) -> a
