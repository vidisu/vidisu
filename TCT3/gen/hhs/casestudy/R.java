/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package hhs.casestudy;

public final class R {
    public static final class array {
        public static final int image_types=0x7f070000;
    }
    public static final class attr {
        /** <p>May be an integer value, such as "<code>100</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
<p>May be one of the following constant values.</p>
<table>
<colgroup align="left" />
<colgroup align="left" />
<colgroup align="left" />
<tr><th>Constant</th><th>Value</th><th>Description</th></tr>
<tr><td><code>any</code></td><td>-1</td><td></td></tr>
<tr><td><code>back</code></td><td>0</td><td></td></tr>
<tr><td><code>front</code></td><td>1</td><td></td></tr>
</table>
         */
        public static final int camera_id=0x7f010001;
        /** <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int show_fps=0x7f010000;
    }
    public static final class drawable {
        public static final int blank=0x7f020000;
        public static final int ic_action_about=0x7f020001;
        public static final int ic_action_search=0x7f020002;
        public static final int ic_action_settings=0x7f020003;
        public static final int ic_av_pause=0x7f020004;
        public static final int ic_av_play=0x7f020005;
        public static final int ic_collections_view_as_grid=0x7f020006;
        public static final int ic_content_edit=0x7f020007;
        public static final int ic_content_redo=0x7f020008;
        public static final int ic_content_remove=0x7f020009;
        public static final int ic_content_save=0x7f02000a;
        public static final int ic_content_undo=0x7f02000b;
        public static final int ic_image_brightness=0x7f02000c;
        public static final int ic_image_crop=0x7f02000d;
        public static final int ic_image_flip_horizontal=0x7f02000e;
        public static final int ic_image_flip_vertical=0x7f02000f;
        public static final int ic_image_hue=0x7f020010;
        public static final int ic_image_redo=0x7f020011;
        public static final int ic_image_rotate_left=0x7f020012;
        public static final int ic_image_rotate_right=0x7f020013;
        public static final int ic_image_save=0x7f020014;
        public static final int ic_image_save_as=0x7f020015;
        public static final int ic_image_transform=0x7f020016;
        public static final int ic_image_undo=0x7f020017;
        public static final int ic_launcher=0x7f020018;
        public static final int ic_navigation_back=0x7f020019;
        public static final int ic_navigation_forward=0x7f02001a;
        public static final int icon=0x7f02001b;
        public static final int img1=0x7f02001c;
        public static final int img2=0x7f02001d;
        public static final int img3=0x7f02001e;
        public static final int img4=0x7f02001f;
    }
    public static final class id {
        public static final int FrameLayout1=0x7f05001a;
        public static final int LinearLayoutBrightnessTools=0x7f05002a;
        public static final int RelativeLayout1=0x7f050048;
        public static final int RelativeLayout2=0x7f050049;
        public static final int TextView02=0x7f050035;
        public static final int any=0x7f050000;
        public static final int back=0x7f050001;
        public static final int btCancel=0x7f050017;
        public static final int btOK=0x7f050016;
        public static final int btnCancel=0x7f05003c;
        public static final int btnClose=0x7f050007;
        public static final int btnNextImage=0x7f05004c;
        public static final int btnOk=0x7f05003b;
        public static final int btnPlayPause=0x7f05004b;
        public static final int btnPreviousImage=0x7f05004a;
        public static final int btnSave=0x7f050047;
        public static final int camera_image=0x7f05004d;
        public static final int detailListView=0x7f050040;
        public static final int editTextCompressQuality=0x7f050044;
        public static final int editTextFileName=0x7f050042;
        public static final int front=0x7f050002;
        public static final int gridViewWithName=0x7f050041;
        public static final int horizontalScrollView1=0x7f05001b;
        public static final int ibtnBrightness=0x7f05001d;
        public static final int ibtnFlipHorizontal=0x7f050025;
        public static final int ibtnFlipVertical=0x7f050026;
        public static final int ibtnHue=0x7f05001e;
        public static final int ibtnRotateLeft=0x7f050023;
        public static final int ibtnRotateRight=0x7f050024;
        public static final int ibtnTransform=0x7f05001c;
        public static final int imageTaggin=0x7f050014;
        public static final int imageView=0x7f050018;
        public static final int imageView1=0x7f050003;
        public static final int linearLayout2=0x7f050020;
        public static final int linearLayout3=0x7f05002b;
        public static final int linearLayout4=0x7f050027;
        public static final int linearLayoutHueSaturation=0x7f050031;
        public static final int linearLayoutToolsSet=0x7f05001f;
        public static final int linearLayoutTransformTools=0x7f050022;
        public static final int menu_about=0x7f05005b;
        public static final int menu_delete_image=0x7f050060;
        public static final int menu_edit_image=0x7f05005d;
        public static final int menu_image_info=0x7f050054;
        public static final int menu_options=0x7f050050;
        public static final int menu_overwrite=0x7f050052;
        public static final int menu_quit=0x7f050055;
        public static final int menu_redo=0x7f05004f;
        public static final int menu_rename_image=0x7f05005e;
        public static final int menu_reset_all=0x7f050053;
        public static final int menu_save_as=0x7f050051;
        public static final int menu_search=0x7f050056;
        public static final int menu_set_as_wallpaper=0x7f05005f;
        public static final int menu_slide_show_options=0x7f05005c;
        public static final int menu_undo=0x7f05004e;
        public static final int menu_view=0x7f050057;
        public static final int menu_view_grid=0x7f050058;
        public static final int menu_view_list=0x7f050059;
        public static final int menu_view_take_picture=0x7f05005a;
        public static final int seekBarBrightness=0x7f05002d;
        public static final int seekBarCompressQuality=0x7f050045;
        public static final int seekBarContrast=0x7f050030;
        public static final int seekBarHue=0x7f050034;
        public static final int seekBarHueLightness=0x7f050037;
        public static final int seekBarSaturation=0x7f05003a;
        public static final int seekBarTransformDegrees=0x7f050029;
        public static final int spinnerImageTypes=0x7f050043;
        public static final int tableRow1=0x7f050008;
        public static final int tableRow2=0x7f05000b;
        public static final int tableRow3=0x7f05000e;
        public static final int tableRow4=0x7f050011;
        public static final int textView3=0x7f05002e;
        public static final int textView4=0x7f05000c;
        public static final int textView5=0x7f050032;
        public static final int textView6=0x7f05000f;
        public static final int textView7=0x7f050038;
        public static final int textView8=0x7f050012;
        public static final int tv_ContactName=0x7f050006;
        public static final int tv_ContactNumber=0x7f050009;
        public static final int tv_TagName=0x7f050015;
        public static final int txtBrightness=0x7f05002c;
        public static final int txtCompressQualityMax=0x7f050046;
        public static final int txtContent=0x7f050005;
        public static final int txtContrast=0x7f05002f;
        public static final int txtDimensions=0x7f05000a;
        public static final int txtHue=0x7f050033;
        public static final int txtHueLightness=0x7f050036;
        public static final int txtImageDateCreated=0x7f05003e;
        public static final int txtImageDimensions=0x7f05003f;
        public static final int txtImageName=0x7f05003d;
        public static final int txtLastModified=0x7f05000d;
        public static final int txtName=0x7f050019;
        public static final int txtSaturation=0x7f050039;
        public static final int txtSize=0x7f050010;
        public static final int txtTitle=0x7f050004;
        public static final int txtToolsSetName=0x7f050021;
        public static final int txtTranformsDegrees=0x7f050028;
        public static final int txtType=0x7f050013;
    }
    public static final class layout {
        public static final int about=0x7f030000;
        public static final int about_image_activity=0x7f030001;
        public static final int autocomplete_layout=0x7f030002;
        public static final int customdialog=0x7f030003;
        public static final int grid_view_item=0x7f030004;
        public static final int image_editing=0x7f030005;
        public static final int list_view_row=0x7f030006;
        public static final int main_activity=0x7f030007;
        public static final int save_dialog=0x7f030008;
        public static final int slide_show=0x7f030009;
        public static final int snapshot=0x7f03000a;
    }
    public static final class menu {
        public static final int menu_image_editing=0x7f090000;
        public static final int menu_main_activity=0x7f090001;
        public static final int menu_slide_show=0x7f090002;
    }
    public static final class raw {
        public static final int lbpcascade_frontalface=0x7f040000;
    }
    public static final class string {
        public static final int app_name=0x7f060000;
        public static final int menu_about=0x7f060004;
        public static final int menu_delete_image=0x7f06000b;
        public static final int menu_edit_image=0x7f060009;
        public static final int menu_image_info=0x7f060008;
        public static final int menu_options=0x7f060001;
        public static final int menu_overwrite=0x7f06000d;
        public static final int menu_quit=0x7f060012;
        public static final int menu_redo=0x7f06000f;
        public static final int menu_rename_image=0x7f06000a;
        public static final int menu_reset_all=0x7f060011;
        public static final int menu_save_as=0x7f060010;
        public static final int menu_search=0x7f060002;
        public static final int menu_set_as_wallpaper=0x7f06000c;
        public static final int menu_undo=0x7f06000e;
        public static final int menu_view=0x7f060003;
        public static final int menu_view_grid=0x7f060005;
        public static final int menu_view_list=0x7f060007;
        public static final int menu_view_take_picture=0x7f060006;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f080000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f080001;
    }
    public static final class styleable {
        /** Attributes that can be used with a CameraBridgeViewBase.
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #CameraBridgeViewBase_camera_id hhs.casestudy:camera_id}</code></td><td></td></tr>
           <tr><td><code>{@link #CameraBridgeViewBase_show_fps hhs.casestudy:show_fps}</code></td><td></td></tr>
           </table>
           @see #CameraBridgeViewBase_camera_id
           @see #CameraBridgeViewBase_show_fps
         */
        public static final int[] CameraBridgeViewBase = {
            0x7f010000, 0x7f010001
        };
        /**
          <p>This symbol is the offset where the {@link hhs.casestudy.R.attr#camera_id}
          attribute's value can be found in the {@link #CameraBridgeViewBase} array.


          <p>May be an integer value, such as "<code>100</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
<p>May be one of the following constant values.</p>
<table>
<colgroup align="left" />
<colgroup align="left" />
<colgroup align="left" />
<tr><th>Constant</th><th>Value</th><th>Description</th></tr>
<tr><td><code>any</code></td><td>-1</td><td></td></tr>
<tr><td><code>back</code></td><td>0</td><td></td></tr>
<tr><td><code>front</code></td><td>1</td><td></td></tr>
</table>
          @attr name android:camera_id
        */
        public static final int CameraBridgeViewBase_camera_id = 1;
        /**
          <p>This symbol is the offset where the {@link hhs.casestudy.R.attr#show_fps}
          attribute's value can be found in the {@link #CameraBridgeViewBase} array.


          <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name android:show_fps
        */
        public static final int CameraBridgeViewBase_show_fps = 0;
    };
}
