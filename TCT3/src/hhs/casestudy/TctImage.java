package hhs.casestudy;

import java.io.File;
import java.text.SimpleDateFormat;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;


public class TctImage
{
	public static String[] strImageTypes = {
		"BMP"  ,
		"GIF"  ,
		"JPG"  ,
		"JPE"  ,
		"JPEG" ,
		"PNG"  ,
		"TIFF"
	};
	
	public static ImageInfo getImageInfo(String imageFilePath) {
			/* Lấy thông tin của 1 file ảnh trong SDCard*/
	
		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inJustDecodeBounds = true;        /* đặt tùy chọn để hàm decodeFile bên dưới không load ảnh vào mà chỉ lấy thông tin của ảnh*/
		BitmapFactory.decodeFile(imageFilePath, opts);
		
		File file = new File(imageFilePath);
		ImageInfo imageInfo = new ImageInfo();
		
		if (file != null && opts != null) {
			imageInfo.name = file.getName();
			imageInfo.path = file.getPath();
		

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			
			imageInfo.lastModified = sdf.format(file.lastModified()); 
			imageInfo.width = opts.outWidth;
			imageInfo.height = opts.outHeight;
			imageInfo.type = opts.outMimeType;
			imageInfo.size = file.length() / 1000.0;
			
			return imageInfo;
		}
		else {
			return null;
		}
	}
	
	public static ImageInfo getImageInfoAt(Resources res, int resId) {
			/* Lấy thông tin hình ảnh trong Resources.
			 * hàm này chỉ dùng để kiểm thử ảnh trong thư mục res*/
		
		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inJustDecodeBounds = true;
		BitmapFactory.decodeResource(res, resId, opts);
		
		return new ImageInfo(
				"bla bla",
				"bla bla",
				"10/01/2013",
				opts.outWidth,
				opts.outHeight,
				opts.outMimeType);
	}
	
	public static int calculateInSampleSize(BitmapFactory.Options opts, int reqWidth, int reqHeight) {
					/*from: http://developer.android.com*/
		
		final int width = opts.outWidth;
		final int height = opts.outHeight;
		int inSampleSize = 1;
		
		
		if (width > reqWidth || height > reqHeight) {
			final int widthRatio = Math.round((float) width / (float) reqWidth);
			final int heightRatio = Math.round((float) height / (float) height);
			
			inSampleSize = widthRatio > heightRatio ? widthRatio : heightRatio;
		}
		
		return inSampleSize;
	}
	
	public static Bitmap decodeSampledBitmap(Resources res, int resId, int reqWidth, int reqHeight) {
					/*http://developer.android.com*/
					/* lấy hình ảnh trong Resources theo kích thước reqWidth x reqHeight, 
					 * có thể tryền vào reqWidth = 100, reqHeight = 100 để lấy
					 * bản sao chép của hình ảnh có kích thước 100x100 cho tiết kiệm bộ nhớ.*/
		
		final BitmapFactory.Options opts = new BitmapFactory.Options();
		
		opts.inJustDecodeBounds = true;
		BitmapFactory.decodeResource(res, resId, opts);
		
		opts.inSampleSize = calculateInSampleSize(opts, reqWidth, reqHeight);
		opts.inJustDecodeBounds = false;
		
		
		return BitmapFactory.decodeResource(res, resId, opts);
	}
	
	public static Bitmap decodeSampledBitmap(String imageFilePath, int reqWidth, int reqHeight) {
				/* lấy hình ảnh trong SDCard theo kích thước reqWidth x reqHeight, 
				 * có thể tryền vào reqWidth = 100, reqHeight = 100 để lấy
				 * bản sao chép của hình ảnh có kích thước 100x100 cho tiết kiệm bộ nhớ*/
		
		final BitmapFactory.Options opts = new BitmapFactory.Options();
		
		opts.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(imageFilePath, opts);
		
		opts.inSampleSize = calculateInSampleSize(opts, reqWidth, reqHeight);
		opts.inJustDecodeBounds = false;
		
		return BitmapFactory.decodeFile(imageFilePath, opts);
	}
	
	public static Bitmap rotateBitmap(Bitmap src, int degrees) {
		if (src == null)
			return null;
		
		int width = src.getWidth();
		int height = src.getHeight();
		
		Matrix m = new Matrix();
		m.preRotate(degrees, width, height);
		
		return Bitmap.createBitmap(src, 0, 0, width, height, m, false);
	}
	
	public static Bitmap flipBitmap(Bitmap src, int sx, int sy) {
		if (src == null)
			return null;
		
		int width = src.getWidth();
		int height = src.getHeight();
		
		Matrix m = new Matrix();
		m.setScale(sx, sy);
		//m.postTranslate(0, height);
		
		return Bitmap.createBitmap(src, 0, 0, width, height, m, false);
	}
	
	public static Bitmap Brighten(Bitmap src, int brightenOffset) {
		if (src == null)
			return null;
		
		int width = src.getWidth();
		int height = src.getHeight();
		int[] pix = new int [width * height];
		
		src.getPixels(pix, 0, width, 0, 0, width, height);
		
		int index = 0;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				// extract RGB value
				int r = (pix[index] >> 16) & 0xff;
				int g = (pix[index] >> 8) & 0xff;
				int b = (pix[index] & 0xff);
				
				r = Math.max(0, Math.min(255, r + brightenOffset));
				g = Math.max(0, Math.min(255, g + brightenOffset));
				b = Math.max(0, Math.min(255, b + brightenOffset));
				
				// pack RGB value
				pix[index] = 0xff000000 | (r << 16) | (g << 8) | b;
				index++;
			}
		}
		
		Bitmap bmResult = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
		bmResult.setPixels(pix, 0, width, 0, 0, width, height);
		
		return bmResult;
	}
	
	public static Bitmap setSaturation(Bitmap src, float hue, float val, float sat) {
				/*from: http://android-er.blogspot.com/2012/10/adjust-hue-saturation-and-brightness-by.html*/
		
		if (src == null)
			return null;
		
		int width = src.getWidth();
		int height = src.getHeight();
		
		int[] mapSrcColor = new int [width * height];
		
		
		float[] hsv = new float[3];
		
		src.getPixels(mapSrcColor, 0, width, 0, 0, width, height);
		
		int index = 0;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				
				// convert  from Color to HSV
				Color.colorToHSV(mapSrcColor[index], hsv);
				
				// Adjust HSV
				hsv[0] = hsv[0] + hue;
				if (hsv[0] < 0)
					hsv[0] = 0;
				else if (hsv[0] > 360)
					hsv[0] = 360;
				
				
				hsv[1] = hsv[1] + sat;
				if (hsv[1] < 0)
					hsv[1] = 0;
				else if (hsv[1] > 1)
					hsv[1] = 1;
				

				hsv[2] = hsv[2] + sat;
				if (hsv[2] < 0)
					hsv[2] = 0;
				else if (hsv[2] > 1)
					hsv[2] = 1;
				
				// convert back from HSV to Color
				//
				mapSrcColor[index] = Color.HSVToColor(hsv);
				index++;
			}
		}
		
		
		Config destConfig = src.getConfig();
		if (destConfig == null)
			destConfig = Config.RGB_565;
		
		//
		return Bitmap.createBitmap(mapSrcColor, width, height, destConfig);
	}
}