package hhs.casestudy;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;



public class GridAdapter extends BaseAdapter
{
	/* chỉ load ảnh vào List với kích thước 100x100 để tiết kiệm bộ nhớ*/
	public static final int imageViewWidth = 100;
	public static final int imageViewHeight = 100;
	
	@Override
	public int getCount() {
		return ImagesManager.getInstance().getVisibleImagesCount();
	}

	@Override
	public Object getItem(int position) {
		return ImagesManager.getInstance().getImagePathInfo(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			convertView = inflater.inflate(R.layout.grid_view_item, parent, false);
		}
		
		TextView txtName = (TextView) convertView.findViewById(R.id.txtName);
		ImageView imgView = (ImageView) convertView.findViewById(R.id.imageView);

		ImagesManager imagesManager = ImagesManager.getInstance();
		
		txtName.setText(imagesManager.getImagePathInfo(position).fileName);
		
		imgView.setImageBitmap(
				imagesManager.getBitmapFromFile(
						position,
						imageViewWidth,
						imageViewHeight));
		
		return convertView;
	}
	
}