package hhs.casestudy;

import android.graphics.Bitmap;
import android.os.AsyncTask;



public class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {

	@Override
	protected Bitmap doInBackground(String... arg0) {
		Bitmap bm = TctImage.decodeSampledBitmap(
				arg0[0], 
				Integer.parseInt(arg0[1]),
				Integer.parseInt(arg0[2]));
		
		return bm;
	}
	
}