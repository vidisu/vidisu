package hhs.casestudy;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Button;


public class AboutTctActivity extends Activity implements OnClickListener
{
	private TextView txtTitle;
	private TextView txtContent;
	private Button btnClose;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about);
		
		txtTitle = (TextView)findViewById(R.id.txtTitle);
		txtContent = (TextView)findViewById(R.id.txtContent);
		
		btnClose = (Button)findViewById(R.id.btnClose);
		btnClose.setOnClickListener(this);
		
		txtTitle.setText("VIDISU");
		String content = 
				"VIDISU";
		
		txtContent.setText(content);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnClose:
			this.finish();
			break;
		}
	}
}