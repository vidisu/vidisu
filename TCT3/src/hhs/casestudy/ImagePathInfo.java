package hhs.casestudy;

public class ImagePathInfo {
	public String path;
	public String fileName;
	
	public ImagePathInfo() {
	}
	
	public ImagePathInfo(String path, String fileName) {
		this.path = path;
		this.fileName = fileName;
	}
}