package hhs.casestudy;
import java.io.IOException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;



public class SlideShowActivity extends Activity implements OnClickListener
{
	private ImageView imageView;
	
	private ImageButton btnNextImage;
	private ImageButton btnPreviousImage;
	private ImageButton btnPlayPause;
	
	private Bitmap bitmap; /* Sử dụng khi lấy bitmap của hình ảnh*/
	private ImagePathInfo imagePathInfo = null;   /* Thông tin đường dẫn của hình ảnh*/
	
	
	/* Trạng thái của slideShow*/
	public final static byte PLAY = 0;
	public final static byte PAUSE = 1;
	private byte slideShowState = PAUSE;
	private int  mSlideShowTime = 500;     /* Thời gian chuyển ảnh*/
	
	
	private Context mContext = this;
	private Menu mainMenu = null;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.slide_show);
		
		imageView = (ImageView)findViewById(R.id.imageView);
		btnNextImage = (ImageButton)findViewById(R.id.btnNextImage);
		btnPreviousImage = (ImageButton)findViewById(R.id.btnPreviousImage);
		btnPlayPause = (ImageButton)findViewById(R.id.btnPlayPause);
		
		btnNextImage.setOnClickListener(this);
		btnPreviousImage.setOnClickListener(this);
		btnPlayPause.setOnClickListener(this);
		
		// hiển thị ảnh lên imageView
		getSelectedBitmap();
	}
	
	
	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btnNextImage:
			ImagesManager.getInstance().selectNextImage();
			getSelectedBitmap();
			break;
			
		case R.id.btnPlayPause:
			if (slideShowState == PLAY) {
				btnPlayPause.setImageResource(R.drawable.ic_av_play);
				slideShowState = PAUSE;
				
				
				btnNextImage.setVisibility(View.VISIBLE);
				btnPreviousImage.setVisibility(View.VISIBLE);
			}
			else if (slideShowState == PAUSE) {
				slideShowState = PLAY;
				btnPlayPause.setImageResource(R.drawable.ic_av_pause);
				mThread = new Thread(mRunable);
				mThread.start();
				
				btnNextImage.setVisibility(View.GONE);
				btnPreviousImage.setVisibility(View.GONE);
			}
			break;
			
		case R.id.btnPreviousImage:
			ImagesManager.getInstance().selectPreviousImage();
			getSelectedBitmap();
			break;
			
		default:
			break;
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		
		case R.id.menu_delete_image:
			deleteImage();
			return true;
		
		case R.id.menu_image_info:
			if (bitmap == null)
				return false;
			
			Intent aboutImage = new Intent(mContext, AboutImageActivity.class);
			startActivity(aboutImage);
			return true;
			
		case R.id.menu_edit_image:
			if (bitmap == null)
				return false;
			
			Intent editing = new Intent(mContext, EditImageActivity.class);
			startActivity(editing);
			
			return true;
			
			
		case R.id.menu_rename_image:
			renameImage();
			return true;
			
			
		case R.id.menu_set_as_wallpaper:
			try {
				WallpaperManager mngr = WallpaperManager.getInstance(this);
				
				if (bitmap == null)
					return false;
				
				mngr.setBitmap(bitmap);
			} catch (IOException e) {
				Toast.makeText(this, "Không thể đặt làm ảnh nền: \n" + e.getMessage(), Toast.LENGTH_SHORT).show();
				e.printStackTrace();
			}
			
			Toast.makeText(this, "Đã đặt làm ảnh nền", Toast.LENGTH_SHORT).show();
			return true;
			
			
		case R.id.menu_slide_show_options:
			return false;
			
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	Handler mHander = new Handler() {
		
		public void handleMessage(android.os.Message msg) {
			if (slideShowState == PLAY) {
				ImagesManager.getInstance().selectNextImage();
				getSelectedBitmap();
			}
		};
	};
	
	
	Thread mThread = null;
	
	Runnable mRunable = new Runnable () {

		@Override
		public void run() {
			while (slideShowState != PAUSE) {
				try {
					Thread.sleep(mSlideShowTime);
					mHander.sendEmptyMessage(PLAY);
					
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	};
	
	public void getSelectedBitmap() {
			/* Hàm này lấy bitmap của hình ảnh đang được chọn*/
		ImagesManager imgMngr = ImagesManager.getInstance();		
		
		bitmap = imgMngr.getSelectedBitmap(1000, 1000, true);

		
		if (bitmap == null) {
			
			if (imgMngr.getSelectedImagePathInfo() == null) 
				Toast.makeText(this, "Không có hình ảnh nào trong thẻ nhớ", Toast.LENGTH_LONG).show();
			else
				Toast.makeText( this, "Không thể mở được tập tin:" + imgMngr.getSelectedImagePathInfo().path, Toast.LENGTH_LONG).show();
			
			imageView.setImageBitmap(null);
			imagePathInfo = null;
			
			
			if (mainMenu == null) {
				return;
			}
			
			MenuItem menuInfo = mainMenu.findItem(R.id.menu_image_info);
			if (menuInfo != null)
				menuInfo.setEnabled(false);
			
			MenuItem menuEdit = mainMenu.findItem(R.id.menu_edit_image);
			if (menuEdit != null)
				menuEdit.setEnabled(false);
			
			MenuItem menuDelete = mainMenu.findItem(R.id.menu_delete_image);
			if (menuDelete != null)
				menuDelete.setEnabled(false);
			
			MenuItem menuRename = mainMenu.findItem(R.id.menu_rename_image);
			if (menuRename != null)
				menuRename.setEnabled(false);
			
			MenuItem menuSetWallpaper = mainMenu.findItem(R.id.menu_set_as_wallpaper);
			if (menuSetWallpaper != null)
				menuSetWallpaper.setEnabled(false);
		}
		else {
			imageView.setImageBitmap(bitmap);
			imagePathInfo = imgMngr.getSelectedImagePathInfo();
			if (imagePathInfo != null)
				setTitle(imagePathInfo.fileName);
			
			
			// cho phép sử dụng các men item
			if (mainMenu == null)
				return;
			
			MenuItem menuInfo = mainMenu.findItem(R.id.menu_image_info);
			if (menuInfo != null)
				menuInfo.setEnabled(true);
			
			MenuItem menuEdit = mainMenu.findItem(R.id.menu_edit_image);
			if (menuEdit != null)
				menuEdit.setEnabled(true);
			
			MenuItem menuDelete = mainMenu.findItem(R.id.menu_delete_image);
			if (menuDelete != null)
				menuDelete.setEnabled(true);
			
			MenuItem menuRename = mainMenu.findItem(R.id.menu_rename_image);
			if (menuRename != null)
				menuRename.setEnabled(true);
			
			MenuItem menuSetWallpaper = mainMenu.findItem(R.id.menu_set_as_wallpaper);
			if (menuSetWallpaper != null)
				menuSetWallpaper.setEnabled(true);
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_slide_show, menu);
		
		if (menu != null)
			mainMenu = menu;
		
		return true;
	}
	
	public void deleteImage() {
		if (imagePathInfo == null)
			return;
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		final String imageName = imagePathInfo.fileName;
		
		builder.setMessage("Xóa hình ảnh " + imageName + "?")
			.setPositiveButton("Xóa", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					if (ImagesManager.getInstance().deleteSelectedImage() == true) {
						Toast.makeText(mContext, "Đã xóa " + imageName, Toast.LENGTH_SHORT).show();
						getSelectedBitmap();
					}
					else {
						Toast.makeText(mContext, "Không xóa được hình ảnh " + imageName, Toast.LENGTH_SHORT).show();
					}
				}
			})
			.setNegativeButton("Hủy", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
				}
			});
		
		builder.create();
		builder.show();
	}
	
	public void renameImage() {
		if (imagePathInfo == null) {
			return;
		}
	}
}