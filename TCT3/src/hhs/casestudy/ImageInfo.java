package hhs.casestudy;

public class ImageInfo 
{
	public String name;
	public String path;
	public String lastModified;
	public int width;
	public int height;
	public String type;
	public double size;        //KB


	
	public ImageInfo(String name, String path, String dateCreated, int width, int height, String type) {
		this.name = name;
		this.path = path;
		this.lastModified = dateCreated;
		this.width = width;
		this.height = height;
		this.type = type;
	}
	
	public ImageInfo() {
	}
}
