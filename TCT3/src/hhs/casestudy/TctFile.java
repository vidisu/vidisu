package hhs.casestudy;
import java.io.File;
import java.util.Vector;


import android.os.Environment;


public class TctFile 
{
	public static String[] strImageTypes = {
		"BMP"  ,
		"GIF"  ,
		"JPG"  ,
		"JPE"  ,
		"JPEG" ,
		"PNG"  ,
		"TIFF"
	};
	
	public static void getAllImagesPathFromSDCard(Vector<ImagePathInfo> outImagesPath) {
			/*Hàm này lấy đường dẫn của tất cả ảnh trong thẻ nhớ*/
		if (outImagesPath == null)
			outImagesPath = new Vector<ImagePathInfo>();
		
		
		getAllImagesPathFromFile(
				outImagesPath,
				new File(Environment.getExternalStorageDirectory().toString() +  "/DCIM/Camera"));
	}
	
	public static void getAllImagesPathFromFile(Vector<ImagePathInfo> outImagesPath, File file) {
		if (file == null || outImagesPath == null)
			return;
		
		if (file.isDirectory()) {
			File[] files = file.listFiles();
			
			if (files == null)
				return;
			
			int len = files.length;
			for (int i = 0; i < len; i++)
				if (file.isHidden() == false)     /*Không hiển thị tập tin hay thư mục ẩn*/
					getAllImagesPathFromFile(outImagesPath, files[i]);
		}
		else if (file.isFile()) {
			String fileName = file.getName().toUpperCase();
			
			for(String type: strImageTypes)
				if (fileName.contains(type))
					outImagesPath.add(new ImagePathInfo(file.getPath(), file.getName()));
		}
	}
	
	public static Boolean rename(String filePath, String newName, String outNewPath) {
		if (filePath == null || newName == null)
			return false;
		
		outNewPath = "";
		int len = filePath.length();
		int i = 0;
		
		for (i = 0; i < len; i++)
			if (filePath.charAt(i) == '\\' || filePath.charAt(i) == '/' )
				break;
		
		outNewPath = filePath.substring(0, i-1);
		outNewPath += filePath.charAt(i);
		outNewPath.concat(newName);
		
		return renameFile(filePath, outNewPath);
	}
	
	public static Boolean renameFile(String filePath, String newPath) {
		File file = new File(filePath);
		File newFile = new File(newPath);
		
		return file.renameTo(newFile);
	}
	
	public static String getDirectory(String filePath) {
		int len = filePath.length();
		int i = 0;
		
		for (i = len-1; i >= 0; i--)
			if (filePath.charAt(i) == '\\' || filePath.charAt(i) == '/')
				break;
		
		if (i < 0)
			return null;
		else 
			return filePath.substring(0, i);
	}
	
	public static Boolean delete(String filePath) {
		File file = new File(filePath);
		
		return file.delete();
	}
}