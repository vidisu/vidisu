package hhs.casestudy;
import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;



public class EditImageActivity extends Activity implements OnClickListener, OnSeekBarChangeListener
{
	/* Thành phần dữ liệu trên giao diện*/
	private Menu mainMenu;
	private ImageView mainImageView;
	
	private LinearLayout linearLayoutToolsSet;
	private TextView  txtToolsSetName;

	private Button btnOk;
	private Button btnCancel;	
	
	
	/* Xoay và lật ảnh*/
	private LinearLayout linearLayoutTransformTools;
	private ImageButton  ibtnTransform;
	
	private ImageButton  ibtnRotateLeft;
	private ImageButton  ibtnRotateRight;
	private ImageButton  ibtnFlipHozirontal;
	private ImageButton  ibtnFlipVertical;
	
	private TextView txtTranformDegrees;
	private SeekBar barTransformDegrees;
	
	
	
	/* Điều chỉnh độ sáng và độ tương phản*/
	private LinearLayout linearLayoutBrightnesTools;
	private ImageButton ibtnBrightnessContrast;
	private TextView txtBightness;
	private SeekBar barBrightness;
	
	private TextView txtContrast;
	private SeekBar barContrast;
	
	
	
	/* Điều chỉnh màu sắc và độ bão hòa*/
	private LinearLayout linearLayoutHueSaturation;
	private ImageButton ibtnHueSaturation;
	
	private TextView txtHue;
	private SeekBar barHue;
	
	private TextView txtHueLightness;
	private SeekBar barHueLightness;
	
	private TextView txtSaturation;
	private SeekBar barSaturation;
	
	
	/* thành phần dữ liệu xử lý của lớp*/
	private LinearLayout currentLayout = null;
	private int modificationCount = 0;     
	private Bitmap currentBitmap = null;
	private Bitmap tempBitmap = null;

	
	private Queue<Bitmap> historyQueue = new LinkedList<Bitmap>();
	private int currentHistory = -1;
	
	
	private Boolean isCloseCommand = false;
	private Context mContext = this;
	
	
	
	/* Các biến hằng*/
	public final static int historyArrayMaxSize = 5;        // chỉ có thể undo/redo trong 5 bước 
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.image_editing);
		
		initInterface();
		getAndshowSelectedImage();
		
		if (currentBitmap != null) {
			saveBitmapToHistoryQueue();
			modificationCount = 0;
		}
	}
	
	public void initInterface() {
		
		mainImageView = (ImageView) findViewById(R.id.imageView);
		linearLayoutToolsSet = (LinearLayout) findViewById(R.id.linearLayoutToolsSet);
		linearLayoutToolsSet.setVisibility(View.INVISIBLE);
		txtToolsSetName = (TextView) findViewById(R.id.txtToolsSetName);
		
		
		
		btnOk = (Button) findViewById(R.id.btnOk);
		btnOk.setOnClickListener(this);
		
		btnCancel = (Button) findViewById(R.id.btnCancel);
		btnCancel.setOnClickListener(this);
		
		
		
		
		/* Xoay và lật ảnh*/
		linearLayoutTransformTools = (LinearLayout) findViewById(R.id.linearLayoutTransformTools);
		linearLayoutTransformTools.setVisibility(View.GONE);
		
		ibtnTransform = (ImageButton) findViewById(R.id.ibtnTransform);
		ibtnTransform.setOnClickListener(this);
		
		
		ibtnRotateLeft = (ImageButton) findViewById(R.id.ibtnRotateLeft);
		ibtnRotateLeft.setOnClickListener(this);
		ibtnRotateRight = (ImageButton) findViewById(R.id.ibtnRotateRight);
		ibtnRotateRight.setOnClickListener(this);
		ibtnFlipHozirontal = (ImageButton) findViewById(R.id.ibtnFlipHorizontal);
		ibtnFlipHozirontal.setOnClickListener(this);
		ibtnFlipVertical = (ImageButton) findViewById(R.id.ibtnFlipVertical);
		ibtnFlipVertical.setOnClickListener(this);
		
		txtTranformDegrees = (TextView) findViewById(R.id.txtTranformsDegrees);
		
		barTransformDegrees = (SeekBar) findViewById(R.id.seekBarTransformDegrees);
		barTransformDegrees.setOnSeekBarChangeListener(this);
		
		
		
		/* Độ sáng và độ tương phản*/
		linearLayoutBrightnesTools = (LinearLayout) findViewById(R.id.LinearLayoutBrightnessTools);
		linearLayoutBrightnesTools.setVisibility(View.GONE);
		ibtnBrightnessContrast = (ImageButton) findViewById(R.id.ibtnBrightness);
		ibtnBrightnessContrast.setOnClickListener(this);
		
		txtBightness = (TextView) findViewById(R.id.txtBrightness);
		barBrightness = (SeekBar) findViewById(R.id.seekBarBrightness);
		barBrightness.setOnSeekBarChangeListener(this);
		
		txtContrast = (TextView) findViewById(R.id.txtContrast);
		barContrast = (SeekBar) findViewById(R.id.seekBarContrast);
		barContrast.setOnSeekBarChangeListener(this);	
		
		
		/* Màu sách và độ bão hòa*/
		linearLayoutHueSaturation = (LinearLayout) findViewById(R.id.linearLayoutHueSaturation);
		linearLayoutHueSaturation.setVisibility(View.GONE);
		ibtnHueSaturation = (ImageButton) findViewById(R.id.ibtnHue);
		ibtnHueSaturation.setOnClickListener(this);
		
		txtHue = (TextView) findViewById(R.id.txtHue);
		barHue = (SeekBar) findViewById(R.id.seekBarHue);
		barHue.setOnSeekBarChangeListener(this);
		
		txtHueLightness = (TextView) findViewById(R.id.txtHueLightness);
		barHueLightness = (SeekBar) findViewById(R.id.seekBarHueLightness);
		barHueLightness.setOnSeekBarChangeListener(this);
		
		txtSaturation = (TextView) findViewById(R.id.txtSaturation);
		barSaturation = (SeekBar) findViewById(R.id.seekBarSaturation);
		barSaturation.setOnSeekBarChangeListener(this);
	}
	
	@Override
	public void onClick(View view) {
		Bitmap temp2 = null;
		
		switch (view.getId()) {
		
		/* Nhóm đối tượng (kiểu View)*/
		
		case R.id.btnOk:
			linearLayoutToolsSet.setVisibility(View.GONE);
			
			if (tempBitmap != null) {
				if (currentBitmap != null && modificationCount > 0)
					currentBitmap.recycle();
				
				currentBitmap = tempBitmap;
				saveBitmapToHistoryQueue();
				tempBitmap = null;
			}
			return;
			
		case R.id.btnCancel:
			linearLayoutToolsSet.setVisibility(View.GONE);
		
			if (tempBitmap != null) {
				tempBitmap.recycle();
				tempBitmap = null;
				
				mainImageView.setImageBitmap(currentBitmap);
			}
			return;
			
		/* Xoay và lật ảnh*/
		case R.id.ibtnTransform:
			showTransformTools();
			return;
			
			
		case R.id.ibtnRotateLeft:
			if (tempBitmap != null) {
				temp2 = TctImage.rotateBitmap(tempBitmap, -90);
				tempBitmap.recycle();
				tempBitmap = temp2;
			}
			else
				tempBitmap = TctImage.rotateBitmap(currentBitmap, -90);
			
			previewEffect();
			return;
			
			
		case R.id.ibtnRotateRight:
			if (tempBitmap != null) {
				temp2 = TctImage.rotateBitmap(tempBitmap, +90);
				tempBitmap.recycle();
				tempBitmap = temp2;
			}
			else
				tempBitmap = TctImage.rotateBitmap(currentBitmap, +90);

			previewEffect();
			return;
			
		case R.id.ibtnFlipHorizontal:
			if (tempBitmap != null) {
				temp2 = TctImage.flipBitmap(tempBitmap, -1, 1);
				tempBitmap.recycle();
				tempBitmap = temp2;
			}
			else
				tempBitmap = TctImage.flipBitmap(currentBitmap, -1, 1);
			
			previewEffect();
			return;
			
		case R.id.ibtnFlipVertical:
			if (tempBitmap != null) {
				temp2 = TctImage.flipBitmap(tempBitmap, 1, -1);
				tempBitmap.recycle();
				tempBitmap = temp2;
			}
			else
				tempBitmap = TctImage.flipBitmap(currentBitmap, 1, -1);
			
			previewEffect();
			return;
			
			
		/* Điều chỉnh độ sáng và độ tương phản*/
		case R.id.ibtnBrightness:
			showBrightnessTools();
			return;
			
		/* Điều chỉnh màu sắc và độ bão hòa*/
		case R.id.ibtnHue:
			showHueAndSaturationToos();
			return;
		}
	}
	
	private void previewEffect() {
		if (tempBitmap != null) {
			mainImageView.setImageBitmap(tempBitmap);
		}
	}

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch (item.getItemId()) {
		
		case R.id.menu_quit:
			if (modificationCount > 0) {
				AlertDialog.Builder saveDialog = new AlertDialog.Builder(mContext);
				
				saveDialog.setMessage("Bạn có muốn lưu thay đổi lên hình ảnh?")
					.setPositiveButton("Lưu lại", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							isCloseCommand = true;
							saveAs();
						}
					})
					.setNeutralButton("Không lưu", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							finish();
						}
					})
					.setNegativeButton("Hủy bỏ", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
						}
					});
				
				saveDialog.create();
				saveDialog.show();
			}
			else {
				finish();   // không có sự thay đổi nào, đóng không cần xác nhận
			}
			return true;
			
		case R.id.menu_image_info:
			Intent aboutImage = new Intent(mContext, AboutImageActivity.class);
			startActivity(aboutImage);
			return true;
			
		case R.id.menu_redo:
			redo();
			return true;
			
		case R.id.menu_undo:
			undo();
			return true;
			
		case R.id.menu_overwrite:
			
			ImagePathInfo imgPathInfo = ImagesManager.getInstance().getSelectedImagePathInfo();
			
			if (imgPathInfo == null) {
				Toast.makeText(mContext, "Không tìm thấy tập tin.", Toast.LENGTH_SHORT).show();
				return false;
			}
			
			
			try {
				FileOutputStream fileOutStr;
				fileOutStr = new FileOutputStream(imgPathInfo.path);
				BufferedOutputStream bufOutStream = new BufferedOutputStream(fileOutStr);
				currentBitmap.compress(CompressFormat.JPEG, 100, bufOutStream);
				
				bufOutStream.flush();
				bufOutStream.close();
				
			} catch (FileNotFoundException e) {
				Toast.makeText(mContext, "Lỗi: " + e.getMessage(), Toast.LENGTH_SHORT).show();
				
			} catch (IOException e) {
				Toast.makeText(mContext, "Lỗi: " + e.getMessage(), Toast.LENGTH_SHORT).show();
			}
			
			Toast.makeText(mContext, "Đã lưu thay đổi", Toast.LENGTH_SHORT).show();
			modificationCount = 0;
			return true;
			
		case R.id.menu_save_as:
			saveAs();
			return true;
			
			
		case R.id.menu_reset_all:
			getAndshowSelectedImage();
			return true;
			
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	private void saveAs() {
		final Dialog saveAsDialog = new Dialog(mContext);

		saveAsDialog.setContentView(R.layout.save_dialog);
		saveAsDialog.setTitle("Lưu hình ảnh");
		
		final EditText etxtFileName = (EditText) saveAsDialog.findViewById(R.id.editTextFileName);
		final EditText etxtCompressQuality = (EditText) saveAsDialog.findViewById(R.id.editTextCompressQuality);
		final Spinner  spinnerImgTypes = (Spinner) saveAsDialog.findViewById(R.id.spinnerImageTypes);
		final SeekBar  seekBarCompressQuality = (SeekBar) saveAsDialog.findViewById(R.id.seekBarCompressQuality);
		
		Button btnSave = (Button) saveAsDialog.findViewById(R.id.btnSave);
		Button btnCancel = (Button) saveAsDialog.findViewById(R.id.btnCancel);
		
		
		
		ArrayAdapter<CharSequence> spinnerAdapter 
				= ArrayAdapter.createFromResource(mContext, R.array.image_types, android.R.layout.simple_spinner_item); 
		spinnerImgTypes.setAdapter(spinnerAdapter);
		
		seekBarCompressQuality.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
					etxtCompressQuality.setText(Integer.toString(progress));
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
		});
		
		etxtCompressQuality.setText(Integer.toString(seekBarCompressQuality.getProgress()));
		etxtCompressQuality.setFocusable(false);
		
		
		btnSave.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String fileName = etxtFileName.getText().toString();
				
				if (fileName.isEmpty() == false) {
					
					ImagePathInfo imagePathInfo = ImagesManager.getInstance().getSelectedImagePathInfo();
					String imgType = (String)spinnerImgTypes.getSelectedItem();
					String filePath;
					CompressFormat format = CompressFormat.JPEG;
					
					Toast.makeText(mContext, "Kiểu = " + imgType, Toast.LENGTH_SHORT).show();
					
					if (imgType == "JPEG")
						format = CompressFormat.JPEG;
					else if (imgType == "PNG")
						format = CompressFormat.PNG;
					
					if (imagePathInfo == null)
						filePath = Environment.getExternalStorageDirectory().getPath();
					else
						filePath = TctFile.getDirectory(imagePathInfo.path);
					
					
					filePath += "/" + fileName + "." + imgType;
					int compressQuality = seekBarCompressQuality.getProgress();
					

					try {
						FileOutputStream fileOutStr;
						fileOutStr = new FileOutputStream(filePath);
						BufferedOutputStream bufOutputStr = new BufferedOutputStream(fileOutStr);
						currentBitmap.compress(format, compressQuality, bufOutputStr);
					
						bufOutputStr.flush();
						bufOutputStr.close();
						
					} catch (FileNotFoundException e) {
						e.printStackTrace();
						Toast.makeText(mContext, "Lỗi không tìm thấy tập tin: " + e.getMessage(), Toast.LENGTH_SHORT).show();
						
					} catch (IOException e) {
						Toast.makeText(mContext, "Lỗi: " + e.getMessage() + "\n. Vui lòng kiểm tra lại thông tin.", Toast.LENGTH_SHORT).show();
					}
					
					Toast.makeText(mContext, "Đã lưu lại hình ảnh", Toast.LENGTH_SHORT).show();
					modificationCount = 0;
					saveAsDialog.cancel();
					if (isCloseCommand == true)
						finish();
				}
			}
		});
		
		btnCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				saveAsDialog.cancel();
			}
		});
		
		saveAsDialog.show();
	}
	
	private void undo(){
		Iterator<Bitmap> iterator = null;
		Bitmap tempBitmap = null;
		int i = 0;
		
		if (currentHistory == 0) // đến phần tử cũ nhất, không thể undo
			return;
		
		iterator = historyQueue.iterator();
		
		for (i = 0; i <= currentHistory - 1; i++)
			if (iterator.hasNext())
				tempBitmap = iterator.next();
			else
				break;
		
		if (i == currentHistory) {
				
			if (currentBitmap != null)
				currentBitmap.recycle();
			
			currentBitmap = tempBitmap;
			mainImageView.setImageBitmap(currentBitmap);
			
			modificationCount--;
			currentHistory--;
			
			if (currentHistory == 0)
				mainMenu.findItem(R.id.menu_undo).setEnabled(false);
			mainMenu.findItem(R.id.menu_redo).setEnabled(true);
		}
		else {
			Toast.makeText(mContext, "Khong the tiep tuc undo", Toast.LENGTH_SHORT).show();
		}
		
	}
	
	private void redo() {
		Iterator<Bitmap> iterator = null;
		Bitmap tempBitmap = null;
		int i = 0;
		
		
		if (currentHistory == historyArrayMaxSize - 1) // đến phần tử mới nhất, không thể redo tiếp tục
			return;
		
		iterator = historyQueue.iterator();
		for (i = 0; i <= currentHistory; i++)
			if (iterator.hasNext())
				tempBitmap = iterator.next();
			else
				break;
		
		if (i == currentHistory + 1) {
			if (iterator.hasNext()){
				tempBitmap = iterator.next();         // chuyen den dung vi tri can tim
				
				currentBitmap = tempBitmap;
				mainImageView.setImageBitmap(currentBitmap);
				
				currentHistory++;
				modificationCount++;
				
				if (! iterator.hasNext())
					mainMenu.findItem(R.id.menu_redo).setEnabled(false);
				
				mainMenu.findItem(R.id.menu_undo).setEnabled(true);
			}
		}
		else {
			Toast.makeText(mContext, "Khong the tiep tuc redo", Toast.LENGTH_SHORT).show();
		}
	}
	
	private void setMenuItemRedoVisible(Boolean isVisible) {
		if (mainMenu != null) {
			MenuItem item = mainMenu.findItem(R.id.menu_redo);
			if (item != null)
					item.setEnabled(false);
		}
	}
	
	private void saveBitmapToHistoryQueue() {
		if (currentBitmap == null)
			return;
		
		if (currentHistory == historyArrayMaxSize - 1) {
			historyQueue.poll();     // bo di phan tu dau tien (cu nhat) ra khoi hang doi (so buoc lich su luu lai co han)
			setMenuItemRedoVisible(false);
		}
		else
			currentHistory ++;

		
		// đẩy vào hàng đợi lịch sử
		historyQueue.offer(currentBitmap);		
		modificationCount++;         // ghi nhan lai co su thay doi hinh anh
		
		
		// cho phép người dùng chọn undo
		if (mainMenu != null) {
			MenuItem item = mainMenu.findItem(R.id.menu_undo);
				if (item != null)
					item.setEnabled(true);
		}
	}
	
	private void showTransformTools() {
		if (currentLayout != null)
			currentLayout.setVisibility(View.GONE);
		
		linearLayoutToolsSet.setVisibility(View.VISIBLE);
		currentLayout = linearLayoutTransformTools;
		currentLayout.setVisibility(View.VISIBLE);
		
		txtToolsSetName.setText("Xoay và lật ảnh");
		barTransformDegrees.setProgress(90);
		txtTranformDegrees.setText("0");
	}
	
	private void showBrightnessTools() {
		if (currentLayout != null)
			currentLayout.setVisibility(View.GONE);
		
		linearLayoutToolsSet.setVisibility(View.VISIBLE);
		currentLayout = linearLayoutBrightnesTools;
		currentLayout.setVisibility(View.VISIBLE);
		
		txtToolsSetName.setText("Điều chỉnh độ sáng và tương phản:");
		barBrightness.setProgress(255 - 127);
		txtBightness.setText("0");
		
		barContrast.setProgress(255 - 127);
		txtContrast.setText("0");
	}
	
	private void showHueAndSaturationToos() {
		if (currentLayout != null)
			currentLayout.setVisibility(View.GONE);
		
		linearLayoutToolsSet.setVisibility(View.VISIBLE);
		currentLayout = linearLayoutHueSaturation;
		currentLayout.setVisibility(View.VISIBLE);
		
		txtToolsSetName.setText("Điều chỉnh màu sắc và độ bão hòa:");
		barHue.setProgress(180);
		txtHue.setText("0");
		
		barHueLightness.setProgress(100);
		txtHueLightness.setText("0");
		
		barSaturation.setProgress(100);
		txtSaturation.setText("0");
	}
	
	private void getAndshowSelectedImage() {
		Bitmap temp = ImagesManager.getInstance().getSelectedBitmap(0, 0, false);
		
		if (currentBitmap != null && ! currentBitmap.equals(temp))
			currentBitmap.recycle();
		
		currentBitmap = temp;
		
		if (currentBitmap == null) {
			Toast.makeText(mContext, "Không thể mở được hình ảnh", Toast.LENGTH_LONG).show();
		}else {
			if (mainImageView != null)
				mainImageView.setImageBitmap(currentBitmap);
			
			ImagePathInfo imagePathInfo = ImagesManager.getInstance().getSelectedImagePathInfo();
			if (imagePathInfo != null)
				setTitle(imagePathInfo.fileName);
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_image_editing, menu);
		
		mainMenu = menu;
		menu.findItem(R.id.menu_redo).setEnabled(false);
		menu.findItem(R.id.menu_undo).setEnabled(false);
		
		return true;
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		
		if (fromUser == false)
			return;
		
		if (seekBar.equals(barBrightness)) {
			txtBightness.setText(Integer.toString(progress - 127));
			
			if (tempBitmap != null)
				tempBitmap.recycle();
			
			tempBitmap = TctImage.Brighten(currentBitmap, (progress - 127));
			previewEffect();
			
			
		} else if (seekBar.equals(barContrast)) {
			txtContrast.setText(Integer.toString(progress - 127));
			
			
			
		} else if (seekBar.equals(barTransformDegrees)) {
			txtTranformDegrees.setText(Integer.toString(progress - 90));
			
			if (tempBitmap != null)
				tempBitmap.recycle();
			
			tempBitmap = TctImage.rotateBitmap(currentBitmap, progress - 90);
			previewEffect();
			
			
		} else if (seekBar.equals(barHue)) {
			txtHue.setText(Integer.toString(progress - 180));
			
			if (tempBitmap != null)
				tempBitmap.recycle();
			
			tempBitmap = TctImage.setSaturation(
					currentBitmap, 
					(barHue.getProgress() - 180),
					(barHueLightness.getProgress() - 100) / 100.0f, 
					(barSaturation.getProgress() - 100) / 100.0f);
			
			previewEffect();
			
		} else if (seekBar.equals(barHueLightness)) {
			txtHueLightness.setText(Integer.toString(progress - 100));
			
			if (tempBitmap != null)
				tempBitmap.recycle();
			
			tempBitmap = TctImage.setSaturation(
					currentBitmap, 
					(barHue.getProgress() - 180),
					(barHueLightness.getProgress() - 100) / 100.0f, 
					(barSaturation.getProgress() - 100) / 100.0f);
			
			previewEffect();
			
			
		} else if (seekBar.equals(barSaturation)) {
			txtSaturation.setText(Integer.toString(progress - 100));
			
			if (tempBitmap != null)
				tempBitmap.recycle();
			
			tempBitmap = TctImage.setSaturation(
					currentBitmap, 
					(barHue.getProgress() - 180),
					(barHueLightness.getProgress() - 100) / 100.0f, 
					(barSaturation.getProgress() - 100) / 100.0f);
			
			previewEffect();
		}
		
	}
	

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {		
	}
	

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
	}
}