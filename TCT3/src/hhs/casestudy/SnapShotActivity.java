package hhs.casestudy;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
 
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Paint.Style;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.objdetect.CascadeClassifier;
public class SnapShotActivity extends Activity {
	 final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1888;
	    private ImageView mImage;
	    CascadeClassifier mJavaDetector;
	    Uri imageUri                      = null;
	    Rect[] fs;
	    Vector<Bitmap> listfs = new Vector<Bitmap>();
	    Context context = this;
	    
	    private ArrayList<Map<String, String>> mPeopleList;
	    private SimpleAdapter mAdapter;
	    private AutoCompleteTextView mTxtPhoneNo;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.snapshot);
		mPeopleList = new ArrayList<Map<String, String>>();
		mImage = (ImageView) findViewById(R.id.camera_image);
		
            	String fileName = "Camera_Example.jpg";
                
                // Create parameters for Intent with filename
                 
                ContentValues values = new ContentValues();
                 
                values.put(MediaStore.Images.Media.TITLE, fileName);
                 
                values.put(MediaStore.Images.Media.DESCRIPTION,"Image capture by camera");
                 
                // imageUri is the current activity attribute, define and save it for later usage  
                 
                imageUri = getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                
            	
            	Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            	
            	
            	cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                
            	cameraIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
            	
            	
                startActivityForResult(cameraIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE); 
       
	}
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		
		int action = event.getAction();
		switch (action) {
		    case MotionEvent.ACTION_DOWN:
		    // Do your stuff here
		    	int pos = isClick(fs, (int)event.getX(), (int)event.getY());
		    	if( pos != -1)
		    	{
		    		//Toast.makeText(getApplicationContext(), "Taggin", Toast.LENGTH_LONG).show();
		    		doTaggin(pos);
		    	}
		        break;
		    case MotionEvent.ACTION_MOVE:
		    // Do your stuff here
		        break;
		    case MotionEvent.ACTION_UP:
		    // Do your stuff here
		        break;
		    }

		    return true;
		
	}
	
	 private void doTaggin(int i) {
		// TODO Auto-generated method stub
		 final Dialog dialog = new Dialog(context);
		 dialog.setContentView(R.layout.customdialog);
			ImageView image = (ImageView)dialog.findViewById(R.id.imageTaggin);
			//
			
		    PopulatePeopleList();
		    if(mPeopleList.isEmpty() || mPeopleList == null)
		    	return;
		    mTxtPhoneNo = (AutoCompleteTextView)dialog.findViewById(R.id.tv_TagName);
		    mTxtPhoneNo.setText("");
		    if(mTxtPhoneNo == null)
		    {
		    	Toast.makeText(getApplicationContext(), "Taggin", Toast.LENGTH_LONG).show();
		    	return;
		    }
		    mAdapter = new SimpleAdapter(context, mPeopleList, R.layout.autocomplete_layout,
		            new String[] { "Name", "Phone" }, new int[] {
		                    R.id.tv_ContactName, R.id.tv_ContactNumber });
		    if(mAdapter.isEmpty() || mAdapter == null)
		    	return;
		    mTxtPhoneNo.setAdapter(mAdapter);
		    mTxtPhoneNo.setOnItemClickListener(new OnItemClickListener() {
		    	@Override
		    	public void onItemClick(AdapterView<?> av, View arg1,
		    			int index, long arg3) {
		    		// TODO Auto-generated method stub
		    		Map<String, String> map = (Map<String, String>) av.getItemAtPosition(index);

		            String name  = map.get("Name");
		            mTxtPhoneNo.setText(name);
		    	}
			});
			//
	        image.setImageBitmap(listfs.get(i));
			Button dialogButton = (Button)dialog.findViewById(R.id.btCancel);
			// if button is clicked, close the custom dialog
			dialogButton.setOnClickListener(new View.OnClickListener() {				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
			});

			dialog.show();
	}
	private void PopulatePeopleList() {
		// TODO Auto-generated method stub
		mPeopleList.clear();
	    Cursor people = getContentResolver().query(
	            ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
	    Log.e("List",String.valueOf(people.getCount()));
	    if(people == null)
	    	return;
	    while (people.moveToNext()) {
	        String contactName = people.getString(people
	                .getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
	        String contactId = people.getString(people
	                .getColumnIndex(ContactsContract.Contacts._ID));
	        String hasPhone = people
	                .getString(people
	                        .getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

	        if ((Integer.parseInt(hasPhone) > 0)){
	            // You know have the number so now query it like this
	            Cursor phones = getContentResolver().query(
	            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
	            null,
	            ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ contactId,
	            null, null);
	            while (phones.moveToNext()){
	                //store numbers and display a dialog letting the user select which.
	                String phoneNumber = phones.getString(
	                phones.getColumnIndex(
	                ContactsContract.CommonDataKinds.Phone.NUMBER));
	                Map<String, String> NamePhoneType = new HashMap<String, String>();
	                NamePhoneType.put("Name", contactName);
	                NamePhoneType.put("Phone", phoneNumber);
	                    //Then add this map to the list.
	                    mPeopleList.add(NamePhoneType);
	            }
	            phones.close();
	        }
	    }
	    people.close();
	    startManagingCursor(people);
		
	}
	public String getPath(Uri uri) {
		 String selectedImagePath;
		    //1:MEDIA GALLERY --- query from MediaStore.Images.Media.DATA
		    String[] projection = { MediaStore.Images.Media.DATA };
		    Cursor cursor = managedQuery(uri, projection, null, null, null);
		    if(cursor != null){
		        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		        cursor.moveToFirst();
		        selectedImagePath = cursor.getString(column_index);
		    }else{
		        selectedImagePath = null;
		    }

		    if(selectedImagePath == null){
		        //2:OI FILE Manager --- call method: uri.getPath()
		        selectedImagePath = uri.getPath();
		    }
		    return selectedImagePath;
	    }
	 public int isClick(Rect[] faces,int x,int y )
	 {
		 int i = 0;
		 for (Rect cvRect : faces) {
			 RectF rectf = new RectF(cvRect.x, cvRect.y, cvRect.x + cvRect.width, cvRect.y + cvRect.height);
			 if(rectf.contains(x, y))
			 {
				 return i;
			 }
			 i++;
		 }
		 return -1;
	 }
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) { 
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            
           // 
           mImage.setImageURI(imageUri);
           Bitmap thumbnail = BitmapFactory.decodeFile(getPath(imageUri));
            
            //4
            File file = new File(Environment.getExternalStorageDirectory()+File.separator + "image.jpg");
            try {
                file.createNewFile();
                FileOutputStream fo = new FileOutputStream(file);
                //5
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
            if (OpenCVLoader.initDebug()) {
    			Log.e("INFO", "OpenCVLoader success");
    			try {
    				InputStream is = getResources().openRawResource(
    						R.raw.lbpcascade_frontalface);
    				File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);

    				File mCascadeFile = new File(cascadeDir,
    						"lbpcascade_frontalface.xml");
    				FileOutputStream os = new FileOutputStream(mCascadeFile);

    				int read = 0;
    				byte[] bt = new byte[1024];
    				while ((read = is.read(bt)) != -1) {
    					os.write(bt, 0, read);
    				}
    				mJavaDetector = new CascadeClassifier(
    						mCascadeFile.getAbsolutePath());
    				if (mJavaDetector.empty()) {
    					Log.e("ERROR", "Failed to load cascade classifier");
    					mJavaDetector = null;
    				}
    				Mat mat = new Mat();

    				org.opencv.android.Utils.bitmapToMat(thumbnail, mat);
    				MatOfRect faces = new MatOfRect();
    				if (mJavaDetector != null)
    					mJavaDetector.detectMultiScale(mat, faces, 1.1, 4, 2,
    							new Size(0, 0), new Size());
    				fs = faces.toArray();
    				Log.i("INFO", "count: " + fs.length);
    				
    		        Bitmap bitmapResult = Bitmap.createBitmap(thumbnail.getWidth(), thumbnail.getHeight(), thumbnail.getConfig());
    				
    				Paint paint = new Paint();
    				paint.setColor(Color.RED);
    		        paint.setStrokeWidth(4);
    		        paint.setStyle(Style.STROKE);
    		        Canvas c = new Canvas();
    		        
    		        c.setBitmap(bitmapResult);
    		        c.drawBitmap(thumbnail, 0, 0, paint);
    		        listfs.clear();
    				for (Rect cvRect : fs) {
    					Log.e("CLDebug", "rect face: " + cvRect.x + " top: " + cvRect.y + " width: " + cvRect.width + " height: " + cvRect.height);
    			        RectF rectf = new RectF(cvRect.x, cvRect.y, cvRect.x + cvRect.width, cvRect.y + cvRect.height);
    			        Bitmap bm = Bitmap.createBitmap(thumbnail,cvRect.x,cvRect.y,cvRect.width,cvRect.height);
    			        listfs.add(bm);
    			        c.drawRect(rectf, paint);
    				}
    				//draw
    				mImage.setImageBitmap(bitmapResult);
    				
    			} catch (Exception e) {
    				// TODO: handle exception
    			}
    		} else {
    			Log.e("ERROR", "OpenCVLoader fail");
    		}
        }
	}
	
}
