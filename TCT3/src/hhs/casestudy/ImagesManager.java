package hhs.casestudy;
import java.lang.ref.WeakReference;
import java.util.Random;
import java.util.Vector;
import java.util.concurrent.ExecutionException;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;



class ImagesManager
{
	private static ImagesManager mInstance = null;
	
	private Vector<ImagePathInfo> mImagesPathInfo;
	private Boolean[]      mVisibleImages;
	private int            mInvisibleImageCount;
	private Vector<WeakReference<Bitmap>> mBitmaps;
	
	
	private int mSelectedImageIndex;
	private Bitmap mSelectedBitmap;
	
	
	
	private ImagesManager() {
		mImagesPathInfo = new Vector<ImagePathInfo>();
		
		mInvisibleImageCount = 0;
		
		mSelectedImageIndex = -1;
		mSelectedBitmap = null;
		mBitmaps = null;
	}
	
	public static ImagesManager getInstance() {
		if (mInstance == null)
			mInstance = new ImagesManager();
		
		return mInstance;
	}
	
	public int getVisibleImagesCount() {
		if (mImagesPathInfo == null)
			return 0;
		else 
			return mImagesPathInfo.size() - mInvisibleImageCount;
	}
	
	public int getSize() {
		if (mImagesPathInfo == null)
			return 0;
		else 
			return mImagesPathInfo.size();
	}
	
	public Boolean[] getVisibleImagesArray() {
		return mVisibleImages;
	}
	
	public Vector<ImagePathInfo> getImagesPathVector() {
		return mImagesPathInfo;
	}
	
	public int getImagesPathFromSDCard() {
		TctFile.getAllImagesPathFromSDCard(mImagesPathInfo);
		initVisibleImagesArrayAndBitmaps();
		return 0;
	}
	
	public int getImagesPathFromResource() {
		mImagesPathInfo.add(new ImagePathInfo(Integer.toString(R.drawable.img1), "img1.jpg"));
		mImagesPathInfo.add(new ImagePathInfo(Integer.toString(R.drawable.img2), "img2.jpg"));
		mImagesPathInfo.add(new ImagePathInfo(Integer.toString(R.drawable.img3), "img3.jpg"));
		mImagesPathInfo.add(new ImagePathInfo(Integer.toString(R.drawable.img4), "img4.jpg"));
		
		initVisibleImagesArrayAndBitmaps();
		return 0;
	}
	
	private void initVisibleImagesArrayAndBitmaps() {
		if (mImagesPathInfo != null) {
			int len = mImagesPathInfo.size();
			
			
			mVisibleImages = new Boolean[len];
			mBitmaps = new Vector<WeakReference<Bitmap>>();
			
			for (int i = len - 1; i >= 0; i--) {
				mVisibleImages[i] = true;
				mBitmaps.addElement(null);
			}
		}
	}
	
	public int getActualPosition(int position) {
		if (mInvisibleImageCount == 0)
			return position;
		
		int len = mImagesPathInfo.size();
		int actualPos = 0;
		int count = 0;
		
		for (actualPos = 0; actualPos < len; actualPos++) {
			if (mVisibleImages[actualPos] == true) {
				if (count == position)
					return actualPos;
				else
					count++;
			}
		}
		
		return -1;
	}
	
	public void hideImage(int actualPosition) {
		if (actualPosition < 0 || actualPosition >= mImagesPathInfo.size())
			return;
		
		if (mVisibleImages[actualPosition] == true) {
			mVisibleImages[actualPosition] = false;
			mInvisibleImageCount++;
		}
	
	}
	
	public void unhideImage(int actualPosition) {
		
		if (actualPosition < 0 || actualPosition >= mImagesPathInfo.size())
			return;
		
		if (mVisibleImages[actualPosition] == false) {
			mVisibleImages[actualPosition] = true;
			mInvisibleImageCount--;
		}
	}
	
	public ImagePathInfo getImagePathInfo(int position) {
		int actualPosition = getActualPosition(position);
			
		if (actualPosition != -1)
			return mImagesPathInfo.elementAt(actualPosition);
		else
			return null;
	}
	
	public Bitmap getBitmapFromFile(int position, int reqWidth, int reqHeight) {
		int actualPos = getActualPosition(position);
		
		if (actualPos == -1)
			return null;
		
		if (mBitmaps.elementAt(actualPos) == null ||
				(mBitmaps.elementAt(actualPos) != null && mBitmaps.elementAt(actualPos).get() == null)) {
			
			
			BitmapWorkerTask task = new BitmapWorkerTask();
			String[] params = new String[3];
			params[0] = mImagesPathInfo.elementAt(actualPos).path;
			params[1] = Integer.toString(reqWidth);
			params[2] = Integer.toString(reqHeight);
			task.execute(params);
			
			Bitmap bm;
			
			try {
				bm = task.get();
			} catch (InterruptedException e) {
				e.printStackTrace();
				return null;
			} catch (ExecutionException e) {
				e.printStackTrace();
				return null;
			}
			
			WeakReference<Bitmap> bitmapReference = new WeakReference<Bitmap>(bm); 
			mBitmaps.setElementAt(bitmapReference, actualPos);
			
			/*WeakReference<Bitmap> bitmapReference = new WeakReference<Bitmap>( 
					TctImage.decodeSampledBitmap(
							mImagesPathInfo.elementAt(actualPos).path, 
							reqWidth, 
							reqHeight)); 
			
			mBitmaps.setElementAt(bitmapReference, actualPos);*/
		
		}
		
		
		if (mBitmaps.elementAt(actualPos) != null)
			return mBitmaps.elementAt(actualPos).get();
		else
			return null;
	}
	
	public Bitmap getBitmapFromResource(int position, Resources res, int reqWidth, int reqHeight) {
		int actualPos = getActualPosition(position);
		
		if (actualPos == -1)
			return null;
		
		
		if (mBitmaps.elementAt(actualPos) == null ||
				(mBitmaps.elementAt(actualPos) != null && mBitmaps.elementAt(actualPos).get() == null)) {
			
			WeakReference<Bitmap> bitmapReference = new WeakReference<Bitmap>( 
					TctImage.decodeSampledBitmap(
							res, 
							Integer.parseInt(mImagesPathInfo.elementAt(actualPos).path), 
							reqWidth, 
							reqHeight)); 
			
			mBitmaps.setElementAt(bitmapReference, actualPos);
		}
		
		if (mBitmaps.elementAt(actualPos) != null)
			return mBitmaps.elementAt(actualPos).get();
		else
			return null;
	}
	
	public void selectImage(int position) {
		int actualPos = getActualPosition(position);
		
		if (actualPos == -1)
			return;
		
		if (actualPos != mSelectedImageIndex) {
			mSelectedImageIndex = actualPos;
			
			if (mSelectedBitmap != null) {
				mSelectedBitmap.recycle();
				mSelectedBitmap = null;
			}
		}
	}
	
	public void selectNextImage() {
		if (mImagesPathInfo.size() == 0) {
			RecycleSelectedImage();
			return;
		}
		
		int oldIndex = mSelectedImageIndex;
		
		mSelectedImageIndex++;
		if (mSelectedImageIndex >= mImagesPathInfo.size())
			mSelectedImageIndex = 0;
		
		if (oldIndex != mSelectedImageIndex) {
			if (mSelectedBitmap != null) {
				mSelectedBitmap.recycle();
				mSelectedBitmap = null;
			}
		}
	}
	
	public void selectPreviousImage() {
		if (mImagesPathInfo.size() == 0) {
			RecycleSelectedImage();
			return;
		}
		
		int oldIndex = mSelectedImageIndex;
		
		mSelectedImageIndex--;
		if (mSelectedImageIndex < 0)
			mSelectedImageIndex = mImagesPathInfo.size() - 1;
		
		if (oldIndex != mSelectedImageIndex) {
			if (mSelectedBitmap != null) {
				mSelectedBitmap.recycle();
				mSelectedBitmap = null;
			}
		}
	}
	
	
	public void selectRandomImage() {
		if (mImagesPathInfo == null || mImagesPathInfo == null || 
				(mImagesPathInfo != null && mImagesPathInfo.size() == 0)) {
			RecycleSelectedImage();
			return;
		}
		
		int oldIndex = mSelectedImageIndex;
				
		mSelectedImageIndex = new Random().nextInt(mImagesPathInfo.size());
		
		if (oldIndex != mSelectedImageIndex) {
			if (mSelectedBitmap != null) {
				mSelectedBitmap.recycle();
				mSelectedBitmap = null;
			}
		}
	}
	
	public ImagePathInfo getSelectedImagePathInfo() {		
		if (mSelectedImageIndex < 0 || mSelectedImageIndex >= mImagesPathInfo.size())
			return null;
		else
			return mImagesPathInfo.elementAt(mSelectedImageIndex);
	}
	
	public Bitmap getSelectedBitmap(int reqWidth, int reqHeight, Boolean useReqSize) {

		if (mSelectedBitmap != null)
			return mSelectedBitmap;
		
		if (mSelectedImageIndex < 0)
			return null;
		
		if (useReqSize == true) {
			mSelectedBitmap = TctImage.decodeSampledBitmap(
					mImagesPathInfo.elementAt(mSelectedImageIndex).path,
					reqWidth,
					reqHeight);
		}
		else {
			mSelectedBitmap = BitmapFactory.decodeFile(mImagesPathInfo.elementAt(mSelectedImageIndex).path);
		}
		
		return mSelectedBitmap;
	}
	
	public Bitmap getSelectedBitmap(Resources res, int reqWidth, int reqHeight, Boolean useReqSize) {

		if (mSelectedBitmap != null)
			return mSelectedBitmap;
		
		
		if (mSelectedImageIndex < 0)
			return null;
		
		if (useReqSize == true) {
			mSelectedBitmap = TctImage.decodeSampledBitmap(
					res,
					Integer.parseInt(mImagesPathInfo.elementAt(mSelectedImageIndex).path),
					reqWidth,
					reqHeight);
		}
		else {
			mSelectedBitmap = BitmapFactory.decodeResource(
					res,
					Integer.parseInt(mImagesPathInfo.elementAt(mSelectedImageIndex).path));
		}
			
		
		return mSelectedBitmap;
	}
	
	public Boolean deleteSelectedImage() {
		if (mSelectedImageIndex < 0)
			return false;
		
		int temp = mSelectedImageIndex;
		Boolean result = false;
		
		// Xóa tập tin
		RecycleSelectedImage();
		result = TctFile.delete(mImagesPathInfo.elementAt(temp).path);
		
		// Cập nhập vào danh sách trong chương trình
		if (result == true) {
			if (mBitmaps != null) {
				if (mBitmaps.elementAt(temp).get() != null)
					mBitmaps.elementAt(temp).get().recycle();
			
				mBitmaps.remove(temp);
			}
			

			mImagesPathInfo.remove(temp);

			
			int len = mImagesPathInfo.size();
			for (int i = temp; i < len; i++)
				mVisibleImages[i] = mVisibleImages[i+1];
		}
		
		
		// chuyển sang hình ảnh kế tiếp
		mSelectedImageIndex = temp;
		selectNextImage();
		
		return result;
	}
	
	public void RecycleSelectedImage() {
		mSelectedImageIndex = -1;
		
		if (mSelectedBitmap != null) {
			mSelectedBitmap.recycle();
			mSelectedBitmap = null;
		}
	}
}