package hhs.casestudy;
import java.util.Vector;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.SearchView;
import android.widget.ListView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.Toast;



public class MainActivity extends Activity implements OnClickListener, OnItemClickListener 
{	
	private ListView mListView;
	private ListAdapter mListAdapter;
	
	private GridView mGridView;
	private GridAdapter mGridAdapter;
	
	public ImagesManager imagesManager;
	
	public static String ITEM_CLICKED = "Item";
	private Context mContext = this;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_activity);
		
		imagesManager = ImagesManager.getInstance();
		imagesManager.getImagesPathFromSDCard();         /*Lấy đường dẫn của tất cả hình ảnh trong thẻ nhớ vào chương trình*/
		//imagesManager.getImagesPathFromResource();
		imagesManager.RecycleSelectedImage();
		
		// Chế độ xem theo danh sách
		mListView = (ListView) findViewById(R.id.detailListView);
		mListAdapter = new ListAdapter();
		mListView.setAdapter(mListAdapter);
		mListView.setOnItemClickListener(this);
		
		
		// Chế độ xem theo dạng lưới
		mGridView = (GridView) findViewById(R.id.gridViewWithName);
		mGridAdapter = new GridAdapter();
		mGridView.setAdapter(mGridAdapter);
		mGridView.setOnItemClickListener(this);
		
		getSettings();
		
		setTitle("TCT - " + imagesManager.getSize());
		
		if (imagesManager.getSize() == 0) {
			Toast.makeText(this, "Không tìm thấy hình ảnh nào", Toast.LENGTH_LONG).show();
		}
	}
	
	private void getSettings() {
		mListView.setVisibility(View.GONE);
		mGridView.setVisibility(View.VISIBLE);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_main_activity, menu);
		
		final SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();	
		
		searchView.setOnQueryTextListener(new OnQueryTextListener() {  /* khi tìm kiếm hình ảnh*/
			
			@Override
			public boolean onQueryTextSubmit(String query) {
				return false;
			}
			
			@Override
			public boolean onQueryTextChange(String newText) {
				
				Boolean[] visibleItems = ImagesManager.getInstance().getVisibleImagesArray();
				Vector<ImagePathInfo> imagesPathInfo = imagesManager.getImagesPathVector();
				int len = imagesManager.getSize();
				CharSequence keys = searchView.getQuery().toString();
				
				// không kiểm tra khi nhập quá nhiều kí tự
				if (keys.length() > 256) {
					return false;
				}
				
				for (int i = 0; i < len; i++) {
					if (imagesPathInfo.elementAt(i).fileName.contains(keys) == false)
						imagesManager.hideImage(i);
					else
						if (visibleItems[i] == false)
							imagesManager.unhideImage(i);
				}
				
				mListAdapter.notifyDataSetChanged();
				mGridAdapter.notifyDataSetChanged();
				
				return false;
			}
		});
		
		return true;
	}
	
	@Override
	protected void onResume() {
		super.onResume();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		
		case R.id.menu_view_grid:
			mGridView.setVisibility(View.VISIBLE);
			mListView.setVisibility(View.GONE);
			return true;
			
		case R.id.menu_view_list:
			mListView.setVisibility(View.VISIBLE);
			mGridView.setVisibility(View.GONE);
			return true;
			
		case R.id.menu_about:
			Intent intent = new Intent(this, AboutTctActivity.class);
			startActivity(intent);
			return true;
		
		case R.id.menu_view_take_picture:
			Intent intent1 = new Intent(this, SnapShotActivity.class);
		    
		    startActivity(intent1);
			return true;
		
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onClick(View view) {
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		// Mở màn hình slideShow
		ImagesManager.getInstance().selectImage(position);  /* hình ảnh được chọn lưu trong imagesMamanager*/
		Intent intent = new Intent(mContext, SlideShowActivity.class);
		startActivity(intent);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}