package hhs.casestudy;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;



public class AboutImageActivity extends Activity implements OnClickListener 
{
	private TextView txtDimensions;
	private TextView txtLastModified;
	private TextView txtSize;
	private TextView txtImageType;
	private Button   btnClose;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about_image_activity);
		
		txtDimensions = (TextView) findViewById(R.id.txtDimensions);
		txtLastModified = (TextView) findViewById(R.id.txtLastModified);
		txtSize = (TextView) findViewById(R.id.txtSize);
		txtImageType = (TextView) findViewById(R.id.txtType);
		btnClose = (Button) findViewById(R.id.btnClose);
		
		
		btnClose.setOnClickListener(this);
		ImagesManager manager = ImagesManager.getInstance();
		ImagePathInfo pathInfo = manager.getSelectedImagePathInfo();
		
		if (pathInfo != null) {
			ImageInfo imageInfo = TctImage.getImageInfo(pathInfo.path);
			
			if (imageInfo != null) {
				setTitle(pathInfo.fileName);
				txtDimensions.setText((Integer.toString(imageInfo.width) + "x" + Integer.toString(imageInfo.height)));
				txtLastModified.setText(imageInfo.lastModified);
				txtSize.setText(Double.toString(imageInfo.size) + "KB");
				txtImageType.setText(imageInfo.type);
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnClose:
			finish();
			break;
		}
	}
}