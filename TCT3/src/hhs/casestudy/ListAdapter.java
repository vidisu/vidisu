package hhs.casestudy;
import java.sql.Date;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class ListAdapter extends BaseAdapter
{	
	/* chỉ load ảnh vào List với kích thước 100x100 để tiết kiệm bộ nhớ*/
	public static final int imageViewWidth = 100;
	public static final int imageViewHeight = 100;
	
	@Override
	public int getCount() {
		return ImagesManager.getInstance().getVisibleImagesCount();
	}

	@Override
	public Object getItem(int position) {
		return ImagesManager.getInstance().getImagePathInfo(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			convertView = inflater.inflate(R.layout.list_view_row, parent, false);
		}
		
		ImageView imgView = (ImageView) convertView.findViewById(R.id.imageView);
		TextView txtName = (TextView) convertView.findViewById(R.id.txtImageName);
		TextView txtDateCreated = (TextView) convertView.findViewById(R.id.txtImageDateCreated);
		TextView txtDimensions = (TextView) convertView.findViewById(R.id.txtImageDimensions);
		
		
		ImagesManager imagesManager = ImagesManager.getInstance(); 
		String imgPath = imagesManager.getImagePathInfo(position).path;
		
		
		if (imgPath != null) {
			/*imgView.setImageBitmap(
					imagesManager.getBitmapFromResource(
							position, 
							imgView.getResources(), 
							imageViewWidth, 
							imageViewHeight));
			
			
			ImagePathInfo info = imagesManager.getImagePathInfo(position);
			txtName.setText(info.fileName);
			txtDateCreated.setText("Sửa lần cuối: 20/11/2012");
			txtDimensions.setText("Độ phân giải: 500x500");
			
			imgView.setImageBitmap(
					imagesManager.getBitmapFromFile(
							position,
							imageViewWidth,
							imageViewHeight));*/
			
			ImageInfo info = TctImage.getImageInfo(imgPath);
			
			txtName.setText(info.name);
			txtDateCreated.setText("Sửa lần cuối: " + info.lastModified);
			txtDimensions.setText("Độ phân giải: " + info.width + "x" + info.height);
		}
		
		return convertView;
	}
}